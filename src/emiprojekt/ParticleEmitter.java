package emiprojekt;

import java.util.ArrayList;
import java.util.Iterator;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

/**
 * 
 * @author Marco Pleines, Peter M�senthin
 * 
 */
public class ParticleEmitter {
	ArrayList<Particle> particles;
	PVector origin;
	PImage defaultTexture;
	PApplet parent;

	// ###################################################
	// ### CONSTRUCTOR ###
	// ###################################################

	ParticleEmitter(PVector location, PApplet parent) {
		this.parent = parent;
		origin = location.get();
		particles = new ArrayList<Particle>();
		defaultTexture = parent.loadImage("funke.png", "png");
	}

	// ###################################################
	// ### METHODS ###
	// ###################################################

	/**
	 * Initializes a new particle object and add it to the particle list
	 */
	public void addParticle() {
		particles.add(new Particle(origin));
	}

	/**
	 * Iterate through the particle list and remove dead
	 */
	public void run() {
		Iterator<Particle> it = particles.iterator();
		while (it.hasNext()) {
			Particle p = it.next();
			p.run();
			if (p.isDead()) {
				it.remove();
			}
		}
	}

	/**
	 * Class for a single particle
	 * 
	 * @author Marco Pleines, Peter M�senthin
	 * 
	 */
	private class Particle {
		PVector location;
		PVector velocity;
		PVector acceleration;
		float lifespan;

		// ###################################################
		// ### CONSTRUCTOR ###
		// ###################################################

		Particle(PVector l) {
			acceleration = new PVector(parent.random(-0.1f, 0.1f),
					parent.random(-0.1f, 0.1f));
			velocity = new PVector(parent.random(-3f, 3f), parent.random(-3f,
					3f));
			location = l.get();
			lifespan = 255.0f;
		}

		// ###################################################
		// ### METHODS ###
		// ###################################################

		void run() {
			update();
			display();
		}

		void update() {
			velocity.add(acceleration);
			location.add(velocity);
			lifespan -= 10.0;
		}

		void display() {
			parent.tint(255, lifespan);
			parent.image(defaultTexture, location.x, location.y);
		}

		// check if particle ran out of lifespan
		boolean isDead() {
			if (lifespan < 0.0) {
				return true;
			} else {
				return false;
			}
		}
	}

}

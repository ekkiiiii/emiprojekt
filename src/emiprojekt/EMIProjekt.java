package emiprojekt;

import processing.core.PApplet;

/**
 * "Einf�hrung in die Medieninformatik - Processing" Projekt von: Peter
 * M�senthin, Jan-Erik B�hr, Marina Pfeffer, Kevin Lapinski, Marco Pleines
 */
public class EMIProjekt extends PApplet {

	// ###################################################
	// ### MEMBER VARIABLES ###
	// ###################################################

	// setup
	int sceneWidth;
	int sceneHeight;

	// camera
	float camRotation = 0;
	boolean moveable = true;

	// bg stuff
	float r, g = 0;
	float b = 255;
	float x, y = 1;
	float z = -1;

	// center
	float boxRotation = 0;
	Facebox ekki;
	Facebox peter;
	Facebox marco;
	Facebox kevin;

	// Rectangle Counter
	float rectCount = 0;

	// ###################################################
	// ### MAIN PROCESSING METHODS ###
	// ###################################################

	// +++ alle Bilder deklarieren

	public EMIProjekt(int width, int height) {
		this.sceneHeight = height;
		this.sceneWidth = width;

	}

	public void setup() {
		size(sceneWidth, sceneHeight, P3D);
		rectMode(CENTER);
		smooth();
		// Alle BIlder Laden
		ekki = new Facebox(this, "ekkivorne.png", "ekkihinten.png",
				"ekkilinks.png", "ekkirechts.png", "ekkioben.png");
		peter = new Facebox(this, "petervorne.png", "peterhinten.png",
				"peterlinks.png", "peterrechts.png", "peteroben.png");
		marco = new Facebox(this, "marcovorne.png", "marcohinten.png",
				"marcolinks.png", "marcorechts.png", "marcooben.png");
		kevin = new Facebox(this, "kevinvorne.png", "kevinhinten.png",
				"kevinlinks.png", "kevinrechts.png", "kevinoben.png");

	}

	public void draw() {
		if (moveable) {
			drawBackground();
		} else {
			background(0);
		}
		lights();
		if (moveable) {
			moveCamera();
		} else if (!moveable) {
			if (camRotation <= 90) {
				drawNameMarco();
			} else if (camRotation > 90 && camRotation <= 180) {
				drawNameKevin();
			} else if (camRotation > 180 && camRotation <= 270) {
				drawNameErik();
			} else if (camRotation > 270 && camRotation <= 360) {
				drawNamePeter();
			}
		}
		drawText();
		drawCenter();
	}

	// ###################################################
	// ### DRAW METHODS ###
	// ###################################################

	/**
	 * No use at current state
	 */
	public void drawText() {
		pushMatrix();
		translate(width / 2 - 50, height / 2 - 200, 0);
		rotateY(radians(-camRotation + 70));
		textSize(32);
		noFill();
		text("Processing", 0, 0);
		translate(0, 50);
		text("lololololo", 0, 0);
		popMatrix();
	}

	private void drawBackground() {
		background(255 - r, 255 - g, 0 + b);
		r += x;
		g += y;
		b += z;
		if (r >= 255) {
			x = -1.1f;
		} else if (r <= 0) {
			x = 1.1f;
		}
		if (g >= 255) {
			y = -1.2f;
		} else if (g <= 0) {
			y = 1.2f;
		}
		if (b <= 0) {
			z = 1.3f;
		} else if (b >= 255) {
			z = -1.3f;
		}

	}

	// Die Richtungen sind die Teile des Gesichtes

	private void drawCenter() {
		// base
		pushMatrix();
		translate(width / 2, height / 2, 0);
		fill(255, 0, 0);
		box(200, 50, 200);

		// box 1
		pushMatrix();
		fill(255, 255, 255);
		translate(-80, -50, -80);
		rotateY(radians(boxRotation));
		ekki.draw();
		popMatrix();

		// box 2
		pushMatrix();
		fill(255, 255, 255);
		translate(80, -50, -80);
		rotateY(radians(boxRotation));
		peter.draw();
		popMatrix();

		// box 3
		pushMatrix();
		fill(255, 255, 255);
		translate(80, -50, 80);
		rotateY(radians(boxRotation));
		marco.draw();
		popMatrix();

		// box 4
		pushMatrix();
		fill(255, 255, 255);
		translate(-80, -50, 80);
		rotateY(radians(boxRotation));
		kevin.draw();
		popMatrix();

		// pop base
		popMatrix();

		boxRotation += 0.5;
	}

	public void drawNamePeter() {
		pushMatrix();
		translate(sceneWidth / 3, sceneHeight - 150);
		rotateY(radians(-camRotation + 90));
		textSize(32);
		text("Peter M�senthin", 0, 0);
		fill(255, 255, 255);
		popMatrix();
	}

	public void drawNameErik() {
		pushMatrix();
		translate(sceneWidth / 3, sceneHeight - 150);
		rotateY(radians(-camRotation + 90));
		textSize(32);
		text("Jan-Erik B�hr", 0, 0);
		fill(255, 255, 255);
		popMatrix();
	}

	public void drawNameKevin() {
		pushMatrix();
		translate(sceneWidth / 3, sceneHeight - 150);
		rotateY(radians(-camRotation + 90));
		textSize(32);
		text("Kevin Lapinski", 0, 0);
		fill(255, 255, 255);
		popMatrix();
	}

	public void drawNameMarco() {
		pushMatrix();
		translate(sceneWidth / 3, sceneHeight - 150);
		rotateY(radians(-camRotation + 90));
		textSize(32);
		text("Marco Pleines", 0, 0);
		fill(0, 0, 0);
		translate(0, -15, 10);
		rect(rectCount, 0, 250, 32);
		popMatrix();
		rectCount++;

	}

	// ###################################################
	// ### SPECIAL METHODS ###
	// ###################################################

	/**
	 * Sets the camera position for each draw
	 */
	private void moveCamera() {
		float orbitRadius = 400 + mouseX;
		float xpos = width / 2 + cos(radians(camRotation)) * orbitRadius;
		float zpos = sin(radians(camRotation)) * orbitRadius;

		camera(xpos, mouseY + (height / 2 - 100) * 0, zpos, width / 2,
				height / 2, 0, 0, 1, 0);
		camRotation += 0.5f;
	}

	/**
	 * Sets movement and background-color-change to false. As Result only the
	 * boxes on the baseplatform are rotating and it becomes visible that the
	 * normal movement is a camera movement
	 */
	public void mouseClicked() {
		moveable = !moveable;
	}

}
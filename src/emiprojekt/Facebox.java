package emiprojekt;

import processing.core.PApplet;
import processing.core.PImage;

public class Facebox {

	PImage vorne;
	PImage hinten;
	PImage links;
	PImage rechts;
	PImage oben;
	PApplet p;

	public Facebox(PApplet parent, String vorne, String hinten, String links,
			String rechts, String oben) {

		this.p = parent;
		this.vorne = p.loadImage("img/" + vorne);
		this.hinten = p.loadImage("img/" + hinten);
		this.links = p.loadImage("img/" + links);
		this.rechts = p.loadImage("img/" + rechts);
		this.oben = p.loadImage("img/" + oben);

	}

	public void draw() {

		wuerfelOben();
		wuerfelVorne();
		wuerfelHinten();
		wuerfelLinks();
		wuerfelRechts();

	}

	public void wuerfelOben() {
		p.beginShape();
		p.texture(oben);
		p.vertex(-20, -25, 20, 0, 0);
		p.vertex(-20, -25, -20, 0, 256);
		p.vertex(20, -25, -20, 256, 256);
		p.vertex(20, -25, 20, 256, 0);
		p.endShape();
	}

	public void wuerfelVorne() {
		p.beginShape();
		p.texture(vorne);
		p.vertex(20, -25, 20, 0, 0);
		p.vertex(20, 25, 20, 0, 256);
		p.vertex(-20, 25, 20, 256, 256);
		p.vertex(-20, -25, 20, 256, 0);
		p.endShape();
	}

	public void wuerfelHinten() {
		p.beginShape();
		p.texture(hinten);
		p.vertex(20, -25, -20, 0, 0);
		p.vertex(20, 25, -20, 0, 256);
		p.vertex(-20, 25, -20, 256, 256);
		p.vertex(-20, -25, -20, 256, 0);
		p.endShape();
	}

	public void wuerfelLinks() {
		p.beginShape();
		p.texture(links);
		p.vertex(20, -25, -20, 0, 0);
		p.vertex(20, 25, -20, 0, 256);
		p.vertex(20, 25, 20, 256, 256);
		p.vertex(20, -25, 20, 256, 0);
		p.endShape();
	}

	public void wuerfelRechts() {
		p.beginShape();
		p.texture(rechts);
		p.vertex(-20, -25, -20, 0, 0);
		p.vertex(-20, 25, -20, 0, 256);
		p.vertex(-20, 25, 20, 256, 256);
		p.vertex(-20, -25, 20, 256, 0);
		p.endShape();

	}

}

package emiprojekt;

public class FrameHelper extends javax.swing.JFrame {

	public static int FRAME_WIDTH = 800;
	public static int FRAME_HEIGHT = 600;

	public FrameHelper() {
		// this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.setBounds(100, 100, FRAME_WIDTH, FRAME_HEIGHT);
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		javax.swing.JPanel panel = new javax.swing.JPanel();
		panel.setBounds(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
		processing.core.PApplet sketch = new EMIProjekt(
				panel.getBounds().width, panel.getBounds().height);
		panel.add(sketch);
		this.add(panel);
		sketch.init();
		this.setVisible(true);
	}

}
